local S = minetest.get_translator(minetest.get_current_modname())


sum_parachute.on_use = function(itemstack, user, pointed_thing)
  if user:get_attach() ~= nil then return itemstack end
  if not minetest.is_creative_enabled(user:get_player_name()) then
    itemstack:take_item()
  end
  local pos = user:get_pos()
  local parachute = minetest.add_entity(pos, "sum_parachute:parachute_ENTITY")
  ent = parachute:get_luaentity()
  minetest.sound_play("sum_parachute_open", {
		gain = 1,
		object = ent.object,
	})
  minetest.after(0.1, function(ent, user)
    local v = user:get_velocity()
    v.y = math.max(v.y, -50)
    sum_parachute.attach_object(ent, user)
    ent.object:set_velocity(v)
    ent.object:set_properties({
			physical = true
		})
  end, ent, user)
  return itemstack
end

minetest.register_craftitem("sum_parachute:parachute", {
	description = S("Parachute"),
	_tt_help = S("Usable"),
	_doc_items_longdesc = S("Can be activated mid air to slow descent."),
	-- _doc_items_usagehelp = how_to_throw,
	inventory_image = "sum_parachute_item.png",
	stack_max = 1,
	groups = { usable = 1, transport = 1 },
	-- on_use = sum_parachute.on_use,
  on_secondary_use = sum_parachute.on_use,
	-- _on_dispense = sum_parachute.on_use,
  _driver = nil,
})

minetest.register_craftitem("sum_parachute:parachute_chute", {
	description = S("Parachute Chute"),
	_tt_help = S("Used in crafting"),
	_doc_items_longdesc = S("Needs to be packed into a bag."),
	-- _doc_items_usagehelp = how_to_throw,
	inventory_image = "sum_parachute_chute.png",
	stack_max = 16,
	groups = { craftitem=1, },
})

minetest.register_craftitem("sum_parachute:parachute_bag", {
	description = S("Parachute Bag"),
	_tt_help = S("Used in crafting"),
	_doc_items_longdesc = S("A bag for parachutes. Pack a parachute into it before use or else it might not slow you down much."),
	-- _doc_items_usagehelp = how_to_throw,
	inventory_image = "sum_parachute_bag.png",
	stack_max = 1,
	groups = { craftitem=1, },
})