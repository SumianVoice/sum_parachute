local S = minetest.get_translator(minetest.get_current_modname())

local mcl = minetest.get_modpath("mcl_core") ~= nil

attachto_player.player = {}

-- Staticdata handling because objects may want to be reloaded
function sum_parachute.get_staticdata(self)
	local itemstack = "sum_parachute:parachute"
	if self._itemstack then
		itemstack = self._itemstack:to_table()
	end
	local data = {
		_lastpos = self._lastpos,
		_age = self._age,
		_itemstack = itemstack,
		_disabled = self._disabled,
	}
	return minetest.serialize(data)
end
function sum_parachute.on_activate(self, staticdata, dtime_s)
	local data = minetest.deserialize(staticdata)
	if data then
		self._lastpos = data._lastpos
		self._age = data._age
		self._disabled = data._disabled
		if self._itemstack == nil and data._itemstack ~= nil then
			self._itemstack = ItemStack(data._itemstack)
		end
	end
	self._sounds = {
		idle = {
			time = 0,
			handle = nil
		},
		boost = {
			time = 0,
			handle = nil
		},
	}
	self._flags = {}
end


sum_parachute.set_attach = function(self)
  if not self._driver then return end
	self.object:set_attach(self._driver, "",
		{x = 0, y = 0, z = 0}, {x = 0, y = 0, z = 0})
end

sum_parachute.attach_object = function(self, obj)
	self._driver = obj
	if self._driver and self._driver:is_player() then
		local old_attacher = attachto_player.player[self._driver:get_player_name()]
		if old_attacher ~= nil then
			old_attacher._disabled = true
			if type(old_attacher._on_detach) == "function" then
				old_attacher._on_detach(old_attacher, nil) end
		end
		attachto_player.player[self._driver:get_player_name()] = self
	end

	sum_parachute.set_attach(self)

	local yaw = self.object:get_yaw()
  if self._driver then
    self.object:set_yaw(minetest.dir_to_yaw(self._driver:get_look_dir()))
  end
end

-- make sure the player doesn't get stuck
minetest.register_on_joinplayer(function(player)
	attachto_player.player[player:get_player_name()] = nil
	playerphysics.remove_physics_factor(player, "gravity", "flight")
	playerphysics.remove_physics_factor(player, "speed", "flight")
end)

minetest.register_on_dieplayer(function(player, reason)
	if attachto_player.player[player:get_player_name()] ~= nil then
		local oldparachute = attachto_player.player[player:get_player_name()]
		sum_parachute.on_death(oldparachute, true)
		attachto_player.player[player:get_player_name()] = nil
	end
end)

sum_parachute.detach_object = function(self, change_pos)
	if self._driver and self._driver:is_player() then

		local name = self._driver:get_player_name()
		if mcl then mcl_player.player_attached[name] = false end

		attachto_player.player[self._driver:get_player_name()] = nil
		if playerphysics then
			playerphysics.remove_physics_factor(self._driver, "gravity", "flight")
			playerphysics.remove_physics_factor(self._driver, "speed", "flight")
		end
	end
	self.object:set_detach()
end


sum_parachute.drop_self = function(self, no_drop)
	local drop_bag = true
	local drop_para = true
	if self._driver and self._driver:is_player() then
		attachto_player.player[self._driver:get_player_name()] = nil
		if minetest.is_creative_enabled(self._driver:get_player_name()) then
			drop_bag = false
			drop_para = false
		elseif not no_drop then
			local inv = self._driver:get_inventory()
			-- error("\n\nTrigger\n\n")
			drop_bag = not (inv:add_item("main", ItemStack("sum_parachute:parachute_bag")):is_empty())
			drop_para = not (inv:add_item("main", ItemStack("sum_parachute:parachute_chute")):is_empty())
		end
	end

	if drop_bag then
		minetest.add_item(self.object:get_pos(), "sum_parachute:parachute_bag")
	end
	if drop_para then
		minetest.add_item(self.object:get_pos(), "sum_parachute:parachute_chute")
	end
end

-- clean up
sum_parachute.on_death = function(self, no_drop)
	if self._flags.removed then return false end
	self._flags.removed = true
	self._disabled = true
	sum_parachute.drop_self(self, no_drop)
  self.object:set_properties({
    physical = false
  })
  minetest.sound_play("sum_parachute_fold", {
		gain = 1,
    object = self.object,
	})
	vel = self.object:get_velocity()
	vel = vector.multiply(vel, 0.8)
  if self._driver then
		minetest.after(0.01, function(vel, driver)
			driver:add_velocity(vel)
		end, vel, self._driver)
    sum_parachute.detach_object(self, false)
  end
	self.object:remove()
end

-- sum_parachute
sum_parachute.get_movement = function(self)
  if not self._driver or not self._driver:is_player() then return vector.new() end
  local ctrl = self._driver:get_player_control()
  if not ctrl then return vector.new() end
	local anim = self._anim.idle

  local dir = self._driver:get_look_dir()
	dir.y = 0
	dir = vector.normalize(dir)

  local forward = 0.7
  local up = 0
  local right = 0
  if ctrl.up then
    forward = 1
		anim = self._anim.up
  elseif ctrl.down then
    forward = -0.2
		anim = self._anim.up
  end
  if ctrl.jump then
    up = 0.3
		anim = self._anim.up
	elseif ctrl.aux1 then
		up = -2
  end
  if ctrl.left then
    right = -0.3
		anim = self._anim.up
	elseif ctrl.right then
		right = 0.3
		anim = self._anim.up
  end

  local v = vector.new()
  v = vector.multiply(dir, forward)

	if right ~= 0 then
		local yaw = minetest.dir_to_yaw(dir)
		yaw = yaw - (right * (math.pi / 2))
		yaw = minetest.yaw_to_dir(yaw)
		v = vector.add(v, yaw)
	end

	v.y = up

	-- self.object:set_animation(anim, 24, 0)

  return v
end

local gravity = 1
local move_speed = 8
sum_parachute.max_use_time = 600
sum_parachute.wear_per_sec = 65535 / sum_parachute.max_use_time
-- warn the player 5 sec before fuel runs out
sum_parachute.wear_warn_level = (sum_parachute.max_use_time - 5) * sum_parachute.wear_per_sec

sum_parachute.on_step = function(self, dtime)
  if self._age < 100 then self._age = self._age + dtime end
	if self._age < 0.0 then return
	else
		if self._driver and not self._flags.set_grav and playerphysics then
			playerphysics.add_physics_factor(self._driver, "gravity", "flight", gravity)
			playerphysics.add_physics_factor(self._driver, "speed", "flight", 0)
			self._flags.set_grav = true
		end
	end
	if not self._flags.ready and self._age < 1 then return end

	if not self._flags.visible then
		self.object:set_properties({is_visible = true})
		self._flags.visible = true
	end

	-- sum_parachute.do_particles(self, dtime)

  local p = self.object:get_pos()
  local fly_node = minetest.get_node(vector.offset(p, 0, -0.1, 0))
  local exit = (self._driver and self._driver:get_player_control().sneak)
            or (self._age > 1 and not self._driver)
	exit = exit or (self._driver and self._driver:get_attach())
	exit = exit or minetest.registered_nodes[fly_node.name].walkable == true
	or (minetest.get_item_group(fly_node.name, "liquid") ~= 0)

  if exit then
    sum_parachute.on_death(self, nil)
    return false
  end

  if self._driver then
    self.object:set_yaw(minetest.dir_to_yaw(self._driver:get_look_dir()))
  end

  local a = vector.new()
	local move_mult = move_speed * dtime
	if self._disabled then move_mult = move_mult / 10 end

	local move_vect = sum_parachute.get_movement(self)
  a = vector.multiply(move_vect, move_mult)

  local vel = self._driver:get_velocity()
	vel.x = vel.x * -0.02
	vel.y = vel.y * -0.06
	vel.z = vel.z * -0.02
	vel = vector.add(a, vel)
  self._driver:add_velocity(vel)
end

local cbsize = 0.3
local parachute_ENTITY = {
	physical = false,
	timer = 0,
  backface_culling = false,
	visual = "mesh",
	mesh = "sum_parachute.b3d",
	textures = {"sum_parachute_texture.png"},
	visual_size = {x=1, y=1, z=1},
	collisionbox = {-cbsize, -0, -cbsize,
                   cbsize,  cbsize,  cbsize},
	pointable = false,

	get_staticdata = sum_parachute.get_staticdata,
	on_activate = sum_parachute.on_activate,
  on_step = sum_parachute.on_step,
	_anim = {
		idle = {x = 0, y = 10},
		up = {x = 20, y = 30},
	},
	is_visible = false,
	_thrower = nil,
  _pilot = nil,
  _age = 0,
	_sounds = nil,
	_itemstack = nil,
	_disabled = false,
	_flags = {},
	_fuel = sum_parachute.max_use_time,
	_on_detach = sum_parachute.on_death,

	_lastpos={},
}

minetest.register_entity("sum_parachute:parachute_ENTITY", parachute_ENTITY)